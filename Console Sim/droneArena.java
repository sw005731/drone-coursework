package droneSimulation;

import droneSimulation.directionEnum.NESW;
import java.util.ArrayList;
import java.util.Random;


public class droneArena {
	private int xmax, ymax;	// establishes the maximum size of the arena
	private drone d;// = new drone(2, 5);		
	Random randomGenerator = new Random();		// creates a random object
	ArrayList<drone> manyDrones;		// creates an array for drone called manyDrones
	
	
	public droneArena(int i, int j) {

		int xmax = i;	// values of i and j are assigned to the positions of x and y
		int ymax = j;
		manyDrones = new ArrayList<drone>();		// enters the x and y of drone into the array, manyDrones, shown from line 21 to 27
		
	}

	
	public drone getDroneAt (int x, int y) {
		for (drone d : manyDrones) {		// uses isHere to check if the current drone does not share x and y with a drone in the array list
			if (d.isHere(x,y) == true){
				return d;	
			}
		} 
		return null;
	}
	
	
    public void showDrones(consoleCanvas c) {	// displays the drones onto the console canvas
        for (drone d : manyDrones) {
            d.displayDrone(c); 
        }
    }

	
	
	public void addDrone() {		// the drone uses randomGenerator to randomly create the x and y
		int tempX = randomGenerator.nextInt(xmax); 
		int tempY = randomGenerator.nextInt(ymax);
        if ((tempX == 0) || (tempX== xmax) || (tempY == 0) || (tempY == ymax)) {
            System.out.println("Drone cannot be placed in this location");
        } else {
            drone d = new drone(tempX, tempY, NESW.getRandomDirection());
            if (getDroneAt(tempX, tempY) == null) {
                manyDrones.add(d);
            } else {
                System.out.println("Drone already exists at this location");
            }
        }	

	}

	
	
	public static int getXSize (droneArena f) {	// gets the value of the x and y of the arena and returns it
		return f.xmax;
	}
	
	public static int getYSize(droneArena f) {
		return f.ymax;
	}
	
	
	
	public String toString() {		// displays the x and y of the arena using a string
        StringBuilder info = new StringBuilder();
        for (drone c : manyDrones)
            info.append(c.toString()).append(" in the arena of size ").append(xmax).append(",").append(ymax).append("\n");
        return info.toString();

	}
	
	public void movingAllDrones(droneArena da) {		// calls moveDrone
		for (drone d : manyDrones) {
			d.moveDrone(da);
		}
	}
	
	
	
	public boolean canGoHere(int x, int y) {	
	     for (drone d : manyDrones) {
	            if ((d.isHere(x, y)) || ((x == 0) || (x == (xmax - 1)) || (y == 0) || (y == (ymax - 1)))) {
	                return false;
	            }
	        }
	        return true;

	}
	
	
	public static void main(String[] args) {	// tests the code for droneArena
		droneArena da = new droneArena (20, 10);
		System.out.println(da.toString());	
	}

}
