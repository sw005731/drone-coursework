package droneSimulation;
import droneSimulation.directionEnum.NESW;

public class drone{
	private int x,y, droneCode;	// drone position, id and how it moves from x,y direction
	private static int droneCount = 0;	// gives each drone a unique number
	private NESW testingDirection;
	
	public drone (int xPos, int yPos, NESW w) {
		droneCode = droneCount++;
		x = xPos;	// sets the default position of the drone
		y = yPos;
		testingDirection = w;
	}
	
	public int getX() {
			return x;	// gets the value of x then returns the value
	}
		
	public int getY() {
		return y;	// gets the value of y then returns the value
	}
		




    public void displayDrone(consoleCanvas c) {	// displays the drone into the console canvas
        c.showIt(x, y, "D");
    }

	
	public void setXY(int nx, int ny) {		// sets the new values of x and y as nx and ny
		x =nx;
		y =ny;
	}

	public String toString(){
		return "Drone " + droneCode + " is at: " + x + ", " + y + " travelling in the " + testingDirection.name() + " direction";	// returns the information of drone in a string using the droneCode, and the x and y variable

	}	
		
	
	public boolean isHere(int mx, int my) {		// responsible for checking if the current drone position is equal to the position of another drone
		return (x == mx) && (y == my);
		}

		
		public void moveDrone(droneArena da) {		// moves the drone in current direction, if not the direction is changed
			int directionX;
			int directionY;
			switch (testingDirection.name()) {
				case "North":
					directionX = -1;
					if ((da.canGoHere(x + directionX, y))) {
						x = y + directionX;
					} else {
						testingDirection = testingDirection.getNextDirection();
					}
					break;
					
				case "East":
					directionY = 1;
					if ((da.canGoHere(x, y + directionY))) {
						y = y + directionY;
					} else {
						testingDirection = testingDirection.getNextDirection();
					}
					break;
					
				case "South":
					directionX = 1;
					if ((da.canGoHere(x + directionX, y))) {
						x = x + directionX;
					} else {
						testingDirection = testingDirection.getNextDirection();
					}
					break;
					
				case "West":
					directionY = -1;
					if ((da.canGoHere(x, y + directionY))) {
						y = y + directionY;
					} else {
						testingDirection = testingDirection.getNextDirection();
					}
					break;
			}
	    }
		/*
		 * checks the direction of the drone
		 */
		
		
		public static void main (String[] args) {
			// tests the drone class
			drone b = new drone (5, 3, NESW.north);		// create the drone
			System.out.println(b.toString());	// print the drone's information
			drone b2 = new drone(8, 12, NESW.south);
			System.out.println(b2.toString());
			drone b3 = new drone(4, 20, NESW.west);
			System.out.println(b3.toString());
			
		}
		
}
