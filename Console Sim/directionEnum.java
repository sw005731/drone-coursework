package droneSimulation;
import java.util.*;


import java.util.Random;

public class directionEnum {
    static Random randomDirection = new Random();

    public enum NESW {north, east, south, west;

        private static final NESW[] val = values();

        public static NESW getRandomDirection() {
            return values()[randomDirection.nextInt(values().length)];
        }

        public NESW getNextDirection() {
            return val[(this.ordinal() + 1) % val.length];
        }
    }
}

