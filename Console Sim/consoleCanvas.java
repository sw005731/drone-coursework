package droneSimulation;

public class consoleCanvas {
	private int rows;
	private int columns;
	private String[][] canvas;	// establishes the constructer
	
	public consoleCanvas(int dx, int dy) {
		rows = dx;
		columns = dy;
		canvas = new String[rows][columns];		// assigns the values of dx and dy to the canvas' multi dimensional array
		
        for (int rows = 0; rows < dx; rows++) {
            for (int columns = 0; columns < dy; columns++) {
                canvas[rows][columns] = "#";	// adds a # to the borders of the canvas
            }

        }
        for (int i = 1; i < dx - 1; i++) {
            for (int j = 1; j < dy - 1; j++) {
                canvas[i][j] = " ";		// adds an empty space to the rest of the canvas
            }

        }

	}

	
	public void showIt(int dx, int dy, String str) {
		canvas[dx+1][dx+1] = str;
	}
	
	public String toString() {
        StringBuilder info = new StringBuilder();
        for (String[] x : canvas) {
            for (String y : x) {
                info.append(y).append(" ");
            }
            info.append("\n");
        }
        return info.toString();
    }

	
	

	public static void main(String[] args) {
		consoleCanvas c = new consoleCanvas (10, 15);	// create a canvas
		c.showIt(4,3,"D");		
		System.out.println(c.toString());				// display result
	}


}
