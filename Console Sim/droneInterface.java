package droneSimulation;

import java.util.Scanner;

public class droneInterface {

		 private Scanner s;								// scanner used for input from user
		 private droneArena myArena;				// arena in which drones are shown
		    /**
		    	 * constructor for DroneInterface
		    	 * sets up scanner used for input and the arena
		    	 * then has main loop allowing user to enter commands
		     */
		    public droneInterface() {
		    	 s = new Scanner(System.in);			// set up scanner for user input
		    	 myArena = new droneArena(20, 6);	// create arena of size 20*6
		    	
		        char ch = ' ';
		        do {
		        	System.out.print("Enter (A)dd drone, get (I)nformation, show the (D)isplay, (M)ove the drones or (E)xit > ");
		        	ch = s.next().charAt(0);
		        	s.nextLine();
		        	switch (ch) {
		    			case 'A' :
		    			case 'a' :
		        			myArena.addDrone();	// add a new drone to arena
		        			break;
		        		case 'I' :
		        		case 'i' :
		        			System.out.print(myArena.toString());
		            		break;
		        		case 'x' : 	ch = 'X';				// when X detected program ends
		        			break;
		        		case 'D' :
		        		case 'd' :
		        			doDisplay();
		        		case 'M' :
		        		case 'm' :
		        			myArena.movingAllDrones(myArena);
		        			doDisplay();
		        					
		        	}
		    		} while (ch != 'X');						// test if end
		        
		       s.close();									// close scanner
		    }
		    
		    void doDisplay() {
		   	 int x = droneArena.getXSize(myArena);
		   	 int y = droneArena.getYSize(myArena);
		   	 consoleCanvas Arena = new consoleCanvas(x,y);
		   	 myArena.showDrones(Arena);
		   	 System.out.println(Arena.toString());
		    	}

		    
			public static void main(String[] args) {
				droneInterface r = new droneInterface();	// just call the interface
			}
}
