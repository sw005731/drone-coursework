package droneGUI;

/**
 * @author Kien Nguyen
 * Ball which gets in way of game ball
 */
public class blockerDrone extends drone {

	/**
	 * 
	 */
	public blockerDrone() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ix
	 * @param iy
	 * @param ir
	 */
	public blockerDrone(double ix, double iy, double ir) {
		super(ix, iy, ir);
		col = 'o';
	}

	@Override
	protected void checkDrone(droneArena d) {
		// TODO Auto-generated method stub
	}

	
	protected String getStrType() {
		return "Blocker";
	}

	@Override
	protected void adjustDrones() {
		// TODO Auto-generated method stub
		
	}

}
