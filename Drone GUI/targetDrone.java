package droneGUI;
/**
 * @author Kien Nguyen
 * The Target drone which you are aiming at
 */
public class targetDrone extends drone {
	private int score;
	/**
	 * 
	 */
	public targetDrone() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ix
	 * @param iy
	 * @param ir
	 */
	public targetDrone(double ix, double iy, double ir) {
		super(ix, iy, ir);
		score = 0;
		col = 'g';
	}

	/** 
	 * checkBall in arena 
	 * @param b BallArena
	 */
	@Override
	protected void checkDrone(droneArena d) {
		if (d.checkHit(this)) score++;			// if been hit, then increase score
	}
	/**
	 * draw drone and display score
	 */
	public void drawDrone(myCanvas mc) {
		super.drawDrone(mc);
		mc.showInt(x, y, score);
	}


	/**
	 * return string defining drone ... here as target
	 */
	protected String getStrType() {
		return "Target";
	}

	@Override
	protected void adjustDrones() {
		// TODO Auto-generated method stub
		
	}
}
