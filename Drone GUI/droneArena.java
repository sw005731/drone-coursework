package droneGUI;
import java.util.ArrayList;
/**
 * @author Kien Nguyen
 * Class for Arena of drones
 */
public class droneArena {
	double xSize, ySize;						// size of arena
	private ArrayList<drone> allDrones;			// array list of all drones in arena
	
	/**
	 * construct an arena
	 */
	droneArena() {
		this(500, 400);			// default size
	}
	/**
	 * construct arena of size xS by yS
	 * @param xS
	 * @param yS
	 */
	droneArena(double xS, double yS){
		xSize = xS;
		ySize = yS;
		allDrones = new ArrayList<drone>();					// list of all drones, initially empty
		allDrones.add(new gameDrone(xS/2, yS/2, 10, 45, 10));	// add game drone
		allDrones.add(new targetDrone(xS/2, 30, 15));			// add target drone
		allDrones.add(new paddleDrone(xS/2, yS-20, 20));		// add paddle
		allDrones.add(new blockerDrone(xS/3, yS/4, 15));		// add blocker
		allDrones.add(new blockerDrone(2*xS/3, yS/4, 15));	// add blocker
	}
	/**
	 * return arena size in x direction
	 * @return
	 */
	public double getXSize() {
		return xSize;
	}
	/**
	 * return arena size in y direction
	 * @return
	 */
	public double getYSize() {
		return ySize;
	}
	/**
	 * draw all drones in the arena into interface bi
	 * @param bi
	 */
	public void drawArena(myCanvas mc) {
		for (drone d : allDrones) d.drawDrone(mc);		// draw all drones
	}
	/**
	 * check all drones .. see if need to change angle of moving drones, etc 
	 */
	public void checkDrone() {
		for (drone d : allDrones) d.checkDrone(this);	// check all drones
	}
	/**
	 * adjust all drones .. move any moving ones
	 */
	public void adjustDrone() {
		for (drone d : allDrones) d.adjustDrones();
	}
	/** 
	 * set the paddle drone at x,y
	 * @param x
	 * @param y
	 */
	public void setPaddle(double x, double y) {
		for (drone d : allDrones)
			if (d instanceof paddleDrone) d.setXY(x, y);
	}
	/**
	 * return list of strings defining each drone
	 * @return
	 */
	public ArrayList<String> describeAll() {
		ArrayList<String> ans = new ArrayList<String>();		// set up empty arraylist
		for (drone d : allDrones) ans.add(d.toString());			// add string defining each drone
		return ans;												// return string list
	}
	/** 
	 * Check angle of drone ... if hitting wall, rebound; if hitting drone, change angle
	 * @param x				drone x position
	 * @param y				y
	 * @param rad			radius
	 * @param ang			current angle
	 * @param notID			identify of drone not to be checked
	 * @return				new angle 
	 */
	public double CheckDroneAngle(double x, double y, double rad, double ang, int notID) {
		double ans = ang;
		if (x < rad || x > xSize - rad) ans = 180 - ans;
			// if drone hit (tried to go through) left or right walls, set mirror angle, being 180-angle
		if (y < rad || y > ySize - rad) ans = - ans;
			// if try to go off top or bottom, set mirror angle
		
		for (drone d : allDrones) 
			if (d.getID() != notID && d.hitting(x, y, rad)) ans = 180*Math.atan2(y-d.getY(), x-d	.getX())/Math.PI;
				// check all drones except one with given id
				// if hitting, return angle between the other drone and this one.
		
		return ans;		// return the angle
	}

	/**
	 * check if the target drone has been hit by another drone
	 * @param target	the target drone
	 * @return 	true if hit
	 */
	public boolean checkHit(drone target) {
		boolean ans = false;
		for (drone b : allDrones)
			if (b instanceof gameDrone && b.hitting(target)) ans = true;
				// try all drones, if gameDrone, check if hitting the target
		return ans;
	}
	
	public void addDrone() {
		allDrones.add(new gameDrone(xSize/2, ySize/2, 10, 60, 5));

	}
}
